<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Red Social Luna</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Clientes
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="agregarCliente.php">Agregar</a></li>
          <li><a href="listarClientes.php">Listar</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Proveedores
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="agregarProveedor.php">Agregar</a></li>
          <li><a href="listarProveedores.php">Listar</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
